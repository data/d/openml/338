# OpenML dataset: grub-damage

https://www.openml.org/d/338

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: R. J. Townsend  
**Source**: [original](http://www.cs.waikato.ac.nz/ml/weka/datasets.html) -   
**Please cite**:   

Grass Grubs and Damage Ranking

Data source:   R. J. Townsend
AgResearch, Lincoln, New Zealand

Grass grubs are one of the major insect pests of pasture in Canterbury and  can cause severe pasture damage and economic loss. Pastoral damage may occur periodically over wide ranging areas. Grass grub populations are often influenced by biotic factors (diseases) and farming practices (such as irrigation and heavy rolling). The objective of the report was to report on grass grub population and damage levels to provide objective estimates of the annual losses caused by grass grubs.

The original machine learning objective was to find a relationship between grass grub numbers, irrigation and damage ranking for the period between 1986 to 1992.

Attribute Information:
1. year_zone - Years 0, 1, 2, 6, 7, 8, 9 divided into three zones: f, m, c - enumerated
2. year - year of trial - enumerated
3. strip - strip of paddock sampled - integer
4. pdk - paddock sampled - integer
5. damage_rankRJT - RJ Townsends damage ranking - enumerated
6. damage_rankALL - other researchers damage ranking - enumerated
7. dry_or_irr - indicates if paddock was dry or irrigated (D: dryland, O: irrigated overhead, B: irrigated border dyke) - enumerated
8. zone - position of paddock (F: foothills, M: midplain, C: coastal) - enumerated
9. GG_new - based on grass grubs per metre squared - enumerated

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/338) of an [OpenML dataset](https://www.openml.org/d/338). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/338/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/338/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/338/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

